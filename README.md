
## Building Docker Image
Clone this repository, navigate to the directory containing the Dockerfile. Run the following command:  
`docker build -t field-pms .`


## Running Docker Image
If you're using Docker desktop, you should see the image in the "Images" tab where you can click the run button. You can also run the following command:  
`docker run -p 8000:80 field-pms`
