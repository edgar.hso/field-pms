<?php
session_start();
require_once('DBConnection.php');

$from = $_POST['from'];
$to = $_POST['to'];
$password = $_POST['password'];

$visitors_sql = "SELECT *, COALESCE((SELECT `fullname` FROM `user_list` where `user_list`.`user_id` = `visitor_list`.`user_id`), 'N/A') as `encoded` FROM `visitor_list` WHERE date(`date_created`) BETWEEN '{$from}' AND '{$to}' ORDER BY strftime('%s', `date_created`) DESC";
$visitors_qry = $conn->query($visitors_sql);

$visitors = [];
while ($row = $visitors_qry->fetchArray(SQLITE3_ASSOC)) {
  $visitors[] = $row;
}

$json_data = json_encode($visitors, JSON_PRETTY_PRINT);

$zip = new ZipArchive();
$temp_file = tempnam('./tmp', 'zip') . '.zip';

if ($zip->open($temp_file, ZipArchive::CREATE | ZipArchive::OVERWRITE) === true) {
  $zip->addFromString('visitors.json', $json_data);
  
  if ($zip->setPassword($password)) {
    $zip->setEncryptionName('visitors.json', ZipArchive::EM_AES_128);
  }

  $zip->close();

  header('Content-Type: application/zip');
  header('Content-Disposition: attachment; filename="visitors.zip"');
  header('Content-Length: ' . filesize($temp_file));

  readfile($temp_file);
  unlink($temp_file);
  exit();
} else {
  echo 'Error creating ZIP file.';
}