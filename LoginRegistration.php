<?php 
session_start();
require_once('DBConnection.php');
Class LoginRegistration extends DBConnection{
    function __construct(){
        parent::__construct();
    }
    function __destruct(){
        parent::__destruct();
    }
    function login(){
        $resp = []; 
    
        $clientIP = $_SERVER['HTTP_X_FORWARDED_FOR'] ?? $_SERVER['REMOTE_ADDR'];
    
        $maxAttempts = 5;
        $cooldownPeriod = 10; // seconds
        $rateLimitKey = "login_attempts_{$clientIP}";
        $currentTime = time();
    
        if(session_status() === PHP_SESSION_NONE) {
            session_start();
        }
    
        if (!isset($_SESSION[$rateLimitKey])) {
            $_SESSION[$rateLimitKey] = ['count' => 0, 'time' => $currentTime];
        }
    
        if ($_SESSION[$rateLimitKey]['count'] >= $maxAttempts) {
            if ($currentTime - $_SESSION[$rateLimitKey]['time'] < $cooldownPeriod) {
                $resp['status'] = 'failed';
                $resp['msg'] = "Too many login attempts. Please try again in a minute.";
                return json_encode($resp);
            } else {
                $_SESSION[$rateLimitKey] = ['count' => 0, 'time' => $currentTime];
            }
        }
    
        extract($_POST);
    
        $allowedToken = $_SESSION['formToken']['login'];
    
        if(!isset($formToken) || (isset($formToken) && $formToken != $allowedToken)){
            $resp['status'] = 'failed';
            $resp['msg'] = "Security Check: Form Token is invalid.";
        } else {
            $sql = "SELECT * FROM user_list WHERE username = '$username'";
            
            $result = $this->query($sql);
    
            $data = $result->fetchArray();
    
            if(!empty($data)){
                $password_verify = password_verify($password, $data['password']);
    
                if($password_verify){
                    if($data['status'] == 1){
                        $resp['status'] = "success";
                        $resp['msg'] = "Login successfully.";
                        foreach($data as $k => $v){
                            if(!is_numeric($k) && !in_array($k, ['password']))
                                $_SESSION[$k] = $v;
                        }
                        
                        $_SESSION[$rateLimitKey]['count'] = 0;
                    }elseif($data['status'] == 0){
                        $resp['status'] = "failed";
                        $resp['msg'] = "Your account is still on subject for approval status.";
                    }elseif($data['status'] == 2){
                        $resp['status'] = "failed";
                        $resp['msg'] = "Your account has been denied to access the system. Please contact the management to settle.";
                    }
                    elseif($data['status'] == 3){
                        $resp['status'] = "failed";
                        $resp['msg'] = "Your account has been blocked. Please contact the management to settle.";
                    }else{
                        $resp['status'] = "failed";
                        $resp['msg'] = "Invalid Status. Please contact the management to settle.";
                    }
                }else{
                    $resp['status'] = "failed";
                    $resp['msg'] = "Invalid username or password.";
                    
                    $_SESSION[$rateLimitKey]['count']++;
                    $_SESSION[$rateLimitKey]['time'] = $currentTime;
                }
            }else{
                $resp['status'] = "failed";
                $resp['msg'] = "Invalid username or password.";
                
                $_SESSION[$rateLimitKey]['count']++;
                $_SESSION[$rateLimitKey]['time'] = $currentTime;
            }
        }
    
        return json_encode($resp);
    }
    
    
    
    function logout(){
        session_destroy();
        header("location:./");
    }
    function register_user(){

        foreach($_POST as $k => $v){
            if(!in_array($k, ['user_id', 'formToken']) && !is_numeric($v) && !is_array($_POST[$k])){
                $_POST[$k] = $this->escapeString($v);
            }
        }
        extract($_POST);
        $allowedToken = $_SESSION['formToken']['registration'];
        if(!isset($formToken) || (isset($formToken) && $formToken = $allowedToken)){
            $resp['status'] = 'failed';
            $resp['msg'] = "Security Check: Form Token is invalid.";
        }else{
            $dbColumn = "(`fullname`, `username`, `password`, `status`, `type`)";
            $password = password_hash($password, PASSWORD_DEFAULT);
            $values = "('{$fullname}', '{$username}', '{$password}', 0, 2)";
            $sql = "INSERT INTO `user_list` {$dbColumn} VALUES {$values}";
            $insert = $this->query($sql);
            if($insert){
                $resp['status'] = 'success';
                $resp['msg'] = "Your Account has been created successfully but it is subject for approval.";

            }else{
                $resp['status'] = 'failed';
                $resp['msg'] = "Error: ".$this->lastErrorMsg();
            }
        }
        echo json_encode($resp);
    }
    function save_user(){
        foreach($_POST as $k => $v){
            if(!in_array($k, ['user_id', 'formToken', 'password']) && !is_numeric($v) && !is_array($_POST[$k])){
                $_POST[$k] = htmlspecialchars($this->escapeString($v));
            }
        }
        extract($_POST);
        $allowedToken = $_SESSION['formToken']['manage_user'];
        if(!isset($formToken) || (isset($formToken) && $formToken != $allowedToken)){
            $resp['status'] = 'failed';
            $resp['msg'] = "Security Check: Form Token is invalid.";
        }else{
            $password = password_hash($password, PASSWORD_DEFAULT);
            if(!empty($user_id)){

                $sql = "UPDATE `user_list` set `fullname` = '{$fullname}', `username` = '{$username}', `password` = '{$password}', `status` = '{$status}', `type` = '{$type}' where `user_id` = '{$user_id}'";
            }else{
                $sql = "INSERT INTO `user_list` (`fullname`, `username`, `password`, `status`, `type`) VALUES ('{$fullname}', '{$username}', '{$password}', '{$status}', '{$type}')";
            }

            $save = $this->query($sql);
            if($save){
                $resp['status'] = 'success';
                if(!empty($user_id)){
                    $resp['msg'] = "User Account has been updated successfully.";
                }else{
                    $resp['msg'] = "User Account has been added successfully.";
                }
                $_SESSION['message']['success'] = $resp['msg'];
            }else{
                $resp['status'] = 'failed';
                $resp['msg'] = "Error: ".$this->lastErrorMsg();
            }
        }
        echo json_encode($resp);
    }
    function update_password(){
        extract($_POST);
        $allowedToken = $_SESSION['formToken']['account-form'];
        if(!isset($formToken) || (isset($formToken) && $formToken != $allowedToken)){
            $resp['status'] = 'failed';
            $resp['msg'] = "Security Check: Form Token is invalid.";
        }else{
            $password = $this->querySingle("SELECT `password` FROM `user_list` where `user_id`='{$_SESSION['user_id']}' ");
            $is_verify = password_verify($current_password, $password);
            if(!$is_verify){
                $resp['status'] = 'failed';
                $resp['msg'] = "Current Password is incorrect.";
            }else{
                if($new_password != $confirm_new_password){
                    $resp['status'] = 'failed';
                    $resp['msg'] = "New Password does not match.";
                }else{
                    $new_password = password_hash($new_password, PASSWORD_DEFAULT);
                    $update = $this->query("UPDATE `user_list` set `password` = '{$new_password}' where `user_id` = '{$_SESSION['user_id']}'");
                    if($update){
                        $resp['status'] = 'success';
                        $resp['msg'] = "Password has been update successfully.";
                        $_SESSION['message']['success'] = $resp['msg'];
                    }else{
                        $resp['status'] = 'failed';
                        $resp['msg'] = $this->lastErrorMsg();
                    }
                }
            }
        }
        echo json_encode($resp);
    }
}
$a = isset($_GET['a']) ?$_GET['a'] : '';
$LG = new LoginRegistration();
switch($a){
    case 'login':
        echo $LG->login();
    break;
    case 'logout':
        echo $LG->logout();
    break;
    case 'register_user':
        echo $LG->register_user();
    break;
    case 'save_user':
        echo $LG->save_user();
    break;
    case 'update_password':
        echo $LG->update_password();
    break;
    default:
    break;
}
