FROM php:7.4-apache

RUN apt-get update && apt-get install -y \
    libsqlite3-dev \
    && docker-php-ext-install pdo pdo_sqlite

RUN a2enmod rewrite


COPY . /var/www/html/


RUN chown -R www-data:www-data /var/www/html/



EXPOSE 80


